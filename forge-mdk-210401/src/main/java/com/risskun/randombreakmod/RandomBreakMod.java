package com.risskun.randombreakmod;

import java.util.List;
import java.util.Random;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("randombreakmod")
public class RandomBreakMod {

  private static final float breakTNTPercent = 0.1f;
  private static final List<BlockState> listUsableBlockState =
      ControleBlocks.createListUsableBlockState();
  private static final BlockState unstableTNTState =
      ControleBlocks.getUnstableTNTState();

  public RandomBreakMod() {
    MinecraftForge.EVENT_BUS.register(this);
  }
  @SubscribeEvent
  public void onBreakSpeed(PlayerEvent.BreakSpeed event) {
    BlockPos posBlock = event.getPos();
    World world = event.getPlayer().getCommandSenderWorld();
    if ((new Random().nextFloat()) < breakTNTPercent ) {
      world.setBlock(posBlock, unstableTNTState, 0);
    } else {
      world.setBlock(posBlock, randList(listUsableBlockState), 0);
    }
  }

  private static <E> E randList(List<E> list) {
    int size = list.size();
    int index = new Random().nextInt(size);
    return list.get(index);
  }
}
