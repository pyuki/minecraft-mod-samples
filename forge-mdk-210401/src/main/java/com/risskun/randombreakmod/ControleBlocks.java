package com.risskun.randombreakmod;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import net.minecraft.block.BlockState;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;

class ControleBlocks {

  static BlockState getUnstableTNTState() {
    List<BlockState> listState = getListBlockStateWithString(Blocks.TNT, ".*unstable=true.*");
    return listState.get(0);
  }

  private static List<BlockState> getListBlockStateWithString(Block block, String str) {
    List<BlockState> listState = block.getStateDefinition().getPossibleStates();
    listState = listState.stream().filter(v -> v.toString().matches(str)).collect(Collectors.toList());
    return listState;
  }

  static List<BlockState> createListUsableBlockState() {
    List<BlockState> list = new ArrayList<BlockState>();

    /* woods */
    list.addAll(Blocks.ACACIA_LEAVES.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.ACACIA_LOG.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.ACACIA_PLANKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.ACACIA_WOOD.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_ACACIA_LOG.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_ACACIA_WOOD.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.BIRCH_LEAVES.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BIRCH_LOG.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BIRCH_PLANKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BIRCH_WOOD.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_BIRCH_LOG.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_BIRCH_WOOD.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.DARK_OAK_LEAVES.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.DARK_OAK_LOG.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.DARK_OAK_PLANKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.DARK_OAK_WOOD.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_DARK_OAK_LOG.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_DARK_OAK_WOOD.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.JUNGLE_LEAVES.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.JUNGLE_LOG.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.JUNGLE_PLANKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.JUNGLE_WOOD.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_JUNGLE_LOG.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_JUNGLE_WOOD.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.OAK_LEAVES.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.OAK_LOG.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.OAK_PLANKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.OAK_WOOD.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_OAK_LOG.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_OAK_WOOD.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.SPRUCE_LEAVES.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SPRUCE_LOG.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SPRUCE_PLANKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SPRUCE_WOOD.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_SPRUCE_LOG.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_SPRUCE_WOOD.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.WARPED_HYPHAE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.WARPED_NYLIUM.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.WARPED_PLANKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.WARPED_STEM.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.WARPED_WART_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_WARPED_HYPHAE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STRIPPED_WARPED_STEM.getStateDefinition().getPossibleStates());

    /* color */
    list.addAll(Blocks.BLACK_WOOL.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BLACKSTONE.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.BLUE_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BLUE_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BLUE_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BLUE_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BLUE_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BLUE_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.BROWN_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BROWN_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BROWN_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BROWN_MUSHROOM_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BROWN_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BROWN_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BROWN_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.CYAN_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CYAN_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CYAN_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CYAN_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CYAN_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CYAN_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.GRAY_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GRAY_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GRAY_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GRAY_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GRAY_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GRAY_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.GREEN_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GREEN_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GREEN_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GREEN_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GREEN_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GREEN_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.LIGHT_BLUE_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIGHT_BLUE_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIGHT_BLUE_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIGHT_BLUE_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIGHT_BLUE_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIGHT_BLUE_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.LIGHT_GRAY_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIGHT_GRAY_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIGHT_GRAY_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIGHT_GRAY_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIGHT_GRAY_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIGHT_GRAY_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.LIME_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIME_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIME_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIME_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIME_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LIME_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.MAGENTA_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.MAGENTA_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.MAGENTA_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.MAGENTA_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.MAGENTA_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.MAGENTA_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.ORANGE_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.ORANGE_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.ORANGE_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.ORANGE_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.ORANGE_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.ORANGE_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.PINK_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.PINK_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.PINK_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.PINK_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.PINK_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.PINK_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.PURPLE_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.PURPLE_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.PURPLE_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.PURPLE_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.PURPLE_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.PURPLE_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.RED_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.RED_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.RED_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.RED_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.RED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.RED_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.WHITE_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.WHITE_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.WHITE_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.WHITE_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.WHITE_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.WHITE_WOOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.YELLOW_CONCRETE_POWDER.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.YELLOW_CONCRETE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.YELLOW_GLAZED_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.YELLOW_STAINED_GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.YELLOW_TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.YELLOW_WOOL.getStateDefinition().getPossibleStates());

    /* BLOCK ORE */
    list.addAll(Blocks.DIAMOND_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.DIAMOND_ORE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.EMERALD_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.EMERALD_ORE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GOLD_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GOLD_ORE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.IRON_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.IRON_ORE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LAPIS_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.LAPIS_ORE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.NETHER_GOLD_ORE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.NETHER_QUARTZ_ORE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.OBSIDIAN.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.REDSTONE_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.REDSTONE_ORE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.REDSTONE_LAMP.getStateDefinition().getPossibleStates());

    /* others */
    list.addAll(Blocks.ANCIENT_DEBRIS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.ANDESITE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BASALT.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BEE_NEST.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BLAST_FURNACE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BONE_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BOOKSHELF.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BRAIN_CORAL_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BRICKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.BUBBLE_CORAL_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CACTUS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CARVED_PUMPKIN.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.CHISELED_NETHER_BRICKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CHISELED_POLISHED_BLACKSTONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CHISELED_QUARTZ_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CHISELED_RED_SANDSTONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CHISELED_SANDSTONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CHISELED_STONE_BRICKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CHORUS_FLOWER.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.CLAY.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.COAL_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.COAL_ORE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.COARSE_DIRT.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.COBBLESTONE.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.CRACKED_NETHER_BRICKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CRACKED_POLISHED_BLACKSTONE_BRICKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CRACKED_STONE_BRICKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CRAFTING_TABLE.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.CRIMSON_HYPHAE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CRIMSON_NYLIUM.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CRIMSON_PLANKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CRIMSON_STEM.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.CRYING_OBSIDIAN.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CUT_RED_SANDSTONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.CUT_SANDSTONE.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.DARK_PRISMARINE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.DEAD_BRAIN_CORAL_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.DEAD_FIRE_CORAL_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.DEAD_HORN_CORAL_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.DEAD_TUBE_CORAL_BLOCK.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.DIORITE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.DIRT.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.DRIED_KELP_BLOCK.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.END_STONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.END_STONE_BRICKS.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.FIRE_CORAL_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GILDED_BLACKSTONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GLASS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GLOWSTONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GRANITE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GRASS_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GRASS_PATH.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.GRAVEL.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.HAY_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.HONEY_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.HONEYCOMB_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.HORN_CORAL_BLOCK.getStateDefinition().getPossibleStates());
    //list.addAll(Blocks.ICE.getStateDefinition().getPossibleStates());
    //list.addAll(Blocks.FROSTED_ICE.getStateDefinition().getPossibleStates());
    //list.addAll(Blocks.BLUE_ICE.getStateDefinition().getPossibleStates());
    //list.addAll(Blocks.PACKED_ICE.getStateDefinition().getPossibleStates());
    
    list.addAll(Blocks.INFESTED_CHISELED_STONE_BRICKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.INFESTED_COBBLESTONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.INFESTED_CRACKED_STONE_BRICKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.INFESTED_STONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.INFESTED_STONE_BRICKS.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.JACK_O_LANTERN.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.MAGMA_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.MELON.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.MOSSY_COBBLESTONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.MOSSY_STONE_BRICKS.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.MUSHROOM_STEM.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.MYCELIUM.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.NETHER_BRICKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.NETHER_WART_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.NETHERRACK.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.PODZOL.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.POLISHED_ANDESITE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.POLISHED_BASALT.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.POLISHED_BLACKSTONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.POLISHED_BLACKSTONE_BRICKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.POLISHED_DIORITE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.POLISHED_GRANITE.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.PRISMARINE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.PRISMARINE_BRICKS.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.PUMPKIN.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.PURPUR_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.PURPUR_PILLAR.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.QUARTZ_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.QUARTZ_BRICKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.QUARTZ_PILLAR.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.RED_MUSHROOM_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.RED_NETHER_BRICKS.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.RED_SAND.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SAND.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SANDSTONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SEA_LANTERN.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SHROOMLIGHT.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SLIME_BLOCK.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.SMOOTH_QUARTZ.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SMOOTH_RED_SANDSTONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SMOOTH_SANDSTONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SMOOTH_STONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SNOW_BLOCK.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SOUL_SAND.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SOUL_SOIL.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.SPONGE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.WET_SPONGE.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.STONE.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.STONE_BRICKS.getStateDefinition().getPossibleStates());

    list.addAll(Blocks.TERRACOTTA.getStateDefinition().getPossibleStates());
    list.addAll(Blocks.TUBE_CORAL_BLOCK.getStateDefinition().getPossibleStates());

    return list;
  }
}
